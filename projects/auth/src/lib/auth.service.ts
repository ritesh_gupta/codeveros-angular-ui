import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {Registration} from './registration.interface';

interface LoginResponse {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private endpoint = 'api/auth';

  redirectUrl: string;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {}

  isLoggedIn() {
    return !!localStorage.getItem('access_token');
  }

  login(username: string, password: string): Observable<boolean> {
    localStorage.removeItem('access_token');
    return this.http.post<LoginResponse>(`${this.endpoint}/login`, {username, password})
      .pipe(map(({ token }) => {
        if (token) {
          localStorage.setItem('access_token', token);
        }
        return !!token;
      }));
  }

  register(registration: Registration): Observable<boolean> {
    return this.http.post<LoginResponse>(`${this.endpoint}/register`, registration)
      .pipe(map( ({ token }) => {
        if (token) {
          localStorage.setItem('access_token', token);
        }
        return !!token;
      }));
  }

  logout() {
    localStorage.removeItem('access_token');
    this.http.post<void>(`${this.endpoint}/logout`, {});
    this.router.navigate(['/login']);
  }
}
