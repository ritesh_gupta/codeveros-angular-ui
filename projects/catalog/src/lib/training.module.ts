import {NgModule} from '@angular/core';
import {TrainingListComponent} from './training-list/training-list.component';
import {TrainingRoutingModule} from './training-routing.module';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule, MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {FlexModule} from '@angular/flex-layout';
import {TrainingDialogComponent} from './training-dialog/training-dialog.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TrainingDeleteDialogComponent} from './training-delete-dialog/training-delete-dialog.component';

@NgModule({
  declarations: [
    TrainingListComponent,
    TrainingDialogComponent,
    TrainingDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    FlexModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    ReactiveFormsModule,
    TrainingRoutingModule
  ],
  exports: [
    TrainingListComponent
  ],
  entryComponents: [
    TrainingDialogComponent,
    TrainingDeleteDialogComponent
  ]
})
export class TrainingModule {}
