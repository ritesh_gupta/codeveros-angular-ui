import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {FlexModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSnackBarModule,
  MatTableModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';

import {UserListComponent} from './user-list/user-list.component';
import {UserDialogComponent} from './user-dialog/user-dialog.component';
import {UserDeleteDialogComponent} from './user-delete-dialog/user-delete-dialog.component';
import {UserRoutingModule} from './user-routing.module';

@NgModule({
  declarations: [
    UserListComponent,
    UserDialogComponent,
    UserDeleteDialogComponent
  ],
  imports: [
    CommonModule,
    FlexModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    MatToolbarModule,
    MatTooltipModule,
    ReactiveFormsModule,
    UserRoutingModule
  ],
  exports: [
    UserListComponent
  ],
  entryComponents: [
    UserDialogComponent,
    UserDeleteDialogComponent
  ]
})
export class UserModule { }
